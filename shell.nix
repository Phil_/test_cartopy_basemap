{ 
  pkgs ? import <nixpkgs> {}
}:
pkgs.mkShell {
  name = "bachelor-shell";
  buildInputs = with pkgs; [
    # base
    python38
    hdfview
    ncview
    netcdf

    # bachelor code
    python38Packages.numpy
    python38Packages.matplotlib
    python38Packages.sphinx
    python38Packages.h5py
    python38Packages.netcdf4
    python38Packages.cftime
    python38Packages.pandas
    python38Packages.basemap

    # for vim
    python38Packages.pynvim
    python38Packages.python-language-server
    python38Packages.jedi
    python38Packages.snowballstemmer
    python38Packages.pluggy
    python38Packages.cartopy
    python38Packages.xarray
  ];
}
