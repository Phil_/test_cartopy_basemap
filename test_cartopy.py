from netCDF4 import Dataset
import matplotlib.pyplot as plt
import h5py
import numpy as np
import time

from mpl_toolkits.basemap import Basemap
import cartopy.crs as ccrs
import cartopy.feature as cfeature


def get_cartopy_plot(nc_file, fig=plt.figure()) -> (plt.figure, plt.axis):
    with Dataset(nc_file, 'r') as df:
        # coordinates
        lats = df.variables['lat']
        lons = df.variables['lon']

        # map projection variables
        cent_lon = df.variables['Lambert_conformal'].longitude_of_central_meridian
        cent_lat = df.variables['Lambert_conformal'].latitude_of_projection_origin
        std_parallel = df.variables['Lambert_conformal'].standard_parallel

        # lower left, upper right points (for the map projection)
        ll = (-lats[0, 0], -lons[0, 0])
        ur = (lats[-1, -1], lons[-1, -1])

        # prep fig and axis
        ax = fig.add_subplot(
            1, 1, 1,
            projection=ccrs.LambertConformal(
                central_longitude=cent_lon,
                central_latitude=cent_lat,
                standard_parallels=std_parallel
            ))
        ax.set_extent([ll[0], ur[0], ll[1], ur[1]], crs=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND)
        ax.add_feature(cfeature.COASTLINE)
        ax.coastlines('10m')
        # ax.stock_img()
        return fig, ax


def get_basemap_plot(nc_file) -> Basemap:
    with Dataset(nc_file, 'r') as df:
        # coordinates
        lats = df.variables['lat']
        lons = df.variables['lon']

        # center points
        lon_0 = df.variables['Lambert_conformal'].longitude_of_central_meridian
        lat_0 = df.variables['Lambert_conformal'].latitude_of_projection_origin
        # standard parallels
        lat_1, lat_2 = df.variables['Lambert_conformal'].standard_parallel

        # lower left, upper right points (for the map projection)
        ll = (lats[0, 0], lons[0, 0])
        ur = (lats[-1, -1], lons[-1, -1])

        m = Basemap(
            # resolution='i',
            projection='lcc',
            lon_0=lon_0, lat_0=lat_0,
            lat_1=lat_1, lat_2=lat_2,
            llcrnrlat=ll[0], llcrnrlon=ll[1],
            urcrnrlat=ur[0], urcrnrlon=ur[1])

        m.drawcoastlines(linewidth=0.4, color="0.4")
        m.drawcountries(linewidth=0.3, color="0.35")
        return m


def main_basemap():
    nc_file = "./ctmout_an16_001_c15.nc"
    m = get_basemap_plot(nc_file)


def main_cartopy():
    nc_file = "./ctmout_an16_001_c15.nc"
    fig, ax = get_cartopy_plot(nc_file)


if __name__ == '__main__':
    main_cartopy()
